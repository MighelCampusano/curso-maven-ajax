import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import com.adalid.appusuarios.modelo.GestionUsuariosBD;

@WebServlet("/usuarios")
public class UsuariosServlet extends HttpServlet {
	
	private GestionUsuariosBD gestUsu;	

    @Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		
        this.srvUsu = new GestionUsuariosBD();
	}

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Recibida peticion GET");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Recibida peticion POST");
        
    }
}
