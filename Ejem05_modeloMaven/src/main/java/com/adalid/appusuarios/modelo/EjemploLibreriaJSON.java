package com.adalid.appusuarios.modelo;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.adalid.appusuarios.modelo.Usuario;
import com.google.gson.Gson;

/* EJERCICIO: Leer fichero CSV con varios usuarios: 
 * 
 * Luis,20
 * Gloria,30
 * Macarena,50
 * */
public class EjemploLibreriaJSON {

	public static void cargarUsuarios() {
		Usuario[] listaCargada;
		String json = "";
		
		try {
	      File myObj = new File("./datos.json");
	      Scanner myReader = new Scanner(myObj);
	      while (myReader.hasNextLine()) {
	        String data = myReader.nextLine();
	        System.out.println(data);
	        json += data;
	      }
	      myReader.close();
	    } catch (FileNotFoundException e) {
	      System.out.println("An error occurred.");
	      e.printStackTrace();
	    }
		
		listaCargada = new Gson().fromJson(json, Usuario[].class);
		for (Usuario usuario : listaCargada) {
			usuario.mostrarDatos();
		}
	}
}
