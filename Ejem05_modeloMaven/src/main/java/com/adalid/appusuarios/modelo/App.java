package com.adalid.appusuarios.modelo;

import java.util.ArrayList;

import com.adalid.appusuarios.modelo.Usuario;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        // EjemploLibreriaJSON.cargarUsuarios();
        try {
			GestionUsuariosBD gestUsu = new GestionUsuariosBD();
			gestUsu.create("Julia", 19, 'M');
			gestUsu.create("Cristhian", 47, 'M');
			gestUsu.create("Heriberto", 23, 'M');
			ArrayList<Usuario> usuarios = gestUsu.mostrarTodos();
			for (Usuario usuario : usuarios) {
				usuario.mostrarDatos();
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
