package com.adalid.appusuarios.modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class GestionUsuariosBD {
	Connection con = null;
	public GestionUsuariosBD() throws ClassNotFoundException {
		super();
		try {			
			Class.forName ("org.h2.Driver"); 
			con = DriverManager.getConnection ("jdbc:h2:~/db_ejemplo", "sa","");
			Statement st = con.createStatement(); 
			st.executeUpdate("CREATE TABLE usuarios ( \r\n" + 
					"   id INT NOT NULL  auto_increment, \r\n" + 
					"   dni VARCHAR(50) NOT NULL, \r\n" + 
					"   edad INT, \r\n" + 
					"   nombre VARCHAR(50) \r\n" + 
					")"); 
			//con.close(); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	public void create(String nombre, int edad, char genero) {
		try {
			Statement st = con.createStatement(); 
			st.executeUpdate("INSERT INTO usuarios ( \r\n" + 
					"   dni, nombre, edad) \r\n" + 
					"   VALUES \r\n" + 
					"   ('SIN DNI', '" + nombre + "', " + edad + ")");
			//con.close(); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	public ArrayList<Usuario>  mostrarTodos() {
        try{
            String sqlQuery = "SELECT * FROM usuarios ";
            // Sentencia preparada para evitar SQL injection
            PreparedStatement sentenciaSQL = con.prepareStatement(sqlQuery);
            ResultSet resultado = sentenciaSQL.executeQuery();
            ArrayList<Usuario> lista = new ArrayList<Usuario>();
            while (resultado.next()) {
                Usuario usu = new Usuario(
                        resultado.getString(4),
                        resultado.getString(2),
                        resultado.getInt(3), 'X');
                usu.id = resultado.getInt(1);
                lista.add(usu);
            }
            return lista;
        } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        return null;
	}
	public void mostrarUnoPorNombre(String nombreABuscar) {
	/*	for (Usuario usuario : listaUsuarios) {
			if (usuario.nombre.equalsIgnoreCase(nombreABuscar)) {
				System.out.println("Encontrado: ");
				usuario.mostrarDatos();
				return;
			}
		}*/
		System.out.println("No se ha encontrado " + nombreABuscar);
	}
	public void borrarUnoPorNombre(String nombreABuscar) {
	}
	
}
