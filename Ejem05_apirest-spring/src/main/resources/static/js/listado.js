/*<tr class="blanco"><td>Aaaaa</td>
                    <td>Activo</td></tr> */

window.onload = function() {
    let objEnvio = new EnvioUsuario();

    let tbody = document.getElementById("contenido-tabla");
    tbody.innerHTML = "";

    function rellenarTabla(arrUsu) {
        let tbodyHTML = "";
        for (let i = 0; i < arrUsu.length; i++) {
            let tr = `<tr class="blanco"><td>${arrUsu[i].id}</td>
                                <td>${arrUsu[i].nombre}</td>
                                <td>${arrUsu[i].edad}</td>
                                <td>${arrUsu[i].estado}</td></tr>
                                <td><input id ="btn_${arrUsu[i].id}" type = "button" value = "Eliminar"></td></tr>`;
            tbodyHTML += tr;
        }
        tbody.innerHTML = tbodyHTML;

        for (let i = 0; i < arrUsu.length; i++) {
            let boton = document.getElementById(`btn_${arrUsu[i].id}`);
            boton.addEventListener("click", function() {
                eliminarUsuario(arrUsu[i].id);
            })
        }
    };
    // Como tbody es local, la función tiene que estar dentro del evento onload
    function traerUsuarios() {
        // Usamos promesas y la función fetch para las llamadas Ajax
        let promesaUsu = fetch("http://localhost:8080/api/usuarios");
        promesaUsu.then(function(respuestaHTTP) {
                return respuestaHTTP.json();
            }).then(function(arrayUsu) {
                console.log(arrayUsu);
                rellenarTabla(arrayUsu);
            })
            .catch(function() {
                alert("Error!");
            });
    };
    traerUsuarios();
};