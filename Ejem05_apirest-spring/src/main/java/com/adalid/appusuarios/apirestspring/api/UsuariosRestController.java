package com.adalid.appusuarios.apirestspring.api;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.*;

import com.adalid.appusuarios.modelo.GestionUsuariosBD;
import com.adalid.appusuarios.modelo.Usuario;

@RestController
@RequestMapping("/api/usuarios")

@CrossOrigin
public class UsuariosRestController {

	GestionUsuariosBD gesUsu;
	
	public UsuariosRestController() throws ClassNotFoundException {
		gesUsu = new GestionUsuariosBD();
	}
	
	// Recepcionamos el método HTTP, POST, para crear un nuevo usuario.
	@PostMapping
	public boolean nuevoUsuario(@RequestBody Usuario usuario) {
		gesUsu.create(usuario.nombre, usuario.edad, usuario.genero);
		return true;
	}
	@GetMapping
	public ArrayList<Usuario> leerTodos() {
		return gesUsu.mostrarTodos();
	}
	
	@DeleteMapping("/{idUsuario}")
	public int eliminaUsuario(@PathVariable int idUsuario) {
		int resultado = gesUsu.eliminarUsuarioPorId(idUsuario);
		return resultado;
	}
	
	
}
