import java.util.HashMap;

public class EjemploHashMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<Integer, Float> mapaInt = new HashMap<>();
		mapaInt.put(10, 3.333f);
		mapaInt.put(15, 4.333f);
		System.out.println("Elem 15 " + mapaInt.get(15));
		
		HashMap<Float, Float> mapaFloat = new HashMap<>();
		mapaFloat.put(1.2345f, 3.333f);
		mapaFloat.put(6.789f, 4.333f);
		System.out.println("Elem 1er " + mapaFloat.get(1.2345f));
		
		HashMap<String, Float> numeros = new HashMap<>();
		numeros.put("PI", 3.141592535f);
		numeros.put("E", 2.73f);
		numeros.put("FI", 1.618f);
		System.out.println("Elem \"PI\" " + numeros.get("PI"));
		System.out.println("Elem \"FI\" " + numeros.get("FI"));
		
	}

}
