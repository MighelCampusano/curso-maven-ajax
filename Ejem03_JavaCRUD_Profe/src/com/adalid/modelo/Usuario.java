package com.adalid.modelo;

/* Las clases son las plantillas de las que se generar�n (intanciar�n) 
 * los distintos objetos con los datos del programa.
 * Es la estructura de la Programaci�n Orienta a Objetos.
 * 
 * P.O.J.O. : Plain Old Java Object
 */
public class Usuario {
	
	// Como una variable global, que s�lo hay 1 para toda la aplicaci�n
	public static int totalUsuarios;
	
	/* Variables miembro (propiedades, atributos, campos...)*/ 
	// TODOS DEBER�AN SER PRIVADOS
	public String nombre;
	public String dni;
	public String telefono;
	public int edad;
	private float salario;
	public boolean estado;
	public char genero;	// H ombre y M ujer	
	// Contructores: "M�todos" especiales para la creaci�n, instanciaci�n o construcci�n de objetos
	// Por defecto, Java crea un contructor sin par�metros
	// Es el m�todo que se llama cuando hacemos new
	public Usuario(String nombre, String dni, int edad, char sexo) {
		// Sirve para inicializar las variables miembro
		this.nombre = nombre;
		this.dni = dni;
		this.edad = edad;
		// this no es obligatoria a menos que haya una variable local con el mismo nombre
		genero = sexo;
		totalUsuarios = totalUsuarios + 1;
	}
	// Para que siga funcionando, al hacer un constructor, java nos obliga  a crear un constructor por defecto.
	public Usuario() {	
		totalUsuarios++;	
	}
		// Funciones miembro, comunes a los objetos, pero propias de cada objeto
	// porque s�lo pueden acceder a sus campos
	// Se llaman m�todos:
	// <tipo_devuelto>  <nombre_metodo> ( <parametros>) { ... <cuerpo> ...}
	public boolean mayorEdad() {
		// this es �ste objeto �Cu�l? El que toque
		return this.edad >= 18; // Con return devolvemos un valor
	}
	public void mostrarDatos() {
		System.out.println("DNI: " + this.dni
				+ "\n - " + this.nombre
				+ "\n - Edad: " + this.edad);
	}
	public void mostrarTodosLosDatos() {
		System.out.println("DNI: " + this.dni
				+ "\n - " + this.nombre
				+ "\n - Edad: " + this.edad
				+ "\n - telefono: " + this.telefono
				+ "\n - salario: " + this.salario
				+ "\n - genero: " + this.genero
				+ "\n - estado: " + ( estado == true ? "activo" : "inactivo" ));		  
	}
	// M�todos con par�metros
	// Los param son datos externos a la funci�n y a la clase que provienen de la llamada al m�todo. Van separados por comas
	public void aumentarSalario(float cantidad) {
		this.salario += cantidad;
		System.out.println("Ha aumentado el salario en " + cantidad + " hasta los " + this.salario + " �");
	}
	// M�todos que reciben algo y devuelven algo
	// Ejemplo: m�todo calcule el salario neto
	
	public float salarioNeto(float irpf, float ss) {
		float impIRPF = this.salario * irpf / 100f;
		float impSS = this.salario * ss / 100f;
		float neto = this.salario - impIRPF - impSS;
		return neto;
	}
	public static void cambiarDNI(Usuario usuCualsea, String nuevoDNI) {
		// Al ser un m�todo independiente de la clase no tiene acceso a los campos, porque no se refiere a ning�n obj en concreto
		// this.dni = nuevoDNI;	ERROR
		if (nuevoDNI.length() > 3 && nuevoDNI.length() < 10)
			usuCualsea.dni = nuevoDNI;
		else
			System.out.println("El DNI " +  nuevoDNI + " en inv�lido");
	}
	public float getSalario() {
		return salario;
	}
	public void setSalario(float salario) {
		this.salario = salario;
	}
	
	
}
