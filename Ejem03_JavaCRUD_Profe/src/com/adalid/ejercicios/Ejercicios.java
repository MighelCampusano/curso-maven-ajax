package com.adalid.ejercicios;

import com.adalid.modelo.GestionarUsuarios;
import com.adalid.modelo.Usuario;

public class Ejercicios {
/*Ejercicio 1: Crear una clase donde gestionar otros usuarios
Ejercicio 2: A�adir m�todo  mostrarTodosLosDatos() que muestre todos los campos del usuarios
Ejercicio 3: M�todo que reciba un String "mujer", "Mujer", "MUJER", "hombre", "hOMbre"...  y cambie el g�nero del usuario
Ejercicio 4: Nuevo constructor con todos los campos
Ejercicio 5: Nueva clase Animal con nombre, especie, genero, peso, edad. Y unos pocos m�todos y contructores que se ocurran, e instanciar 2 animales*/
	
	public static void ejercicio1() {
		Usuario usu3 = new Usuario("Ana", "1111-X", 30, 'M' );
		usu3.estado = true;		
		usu3.mostrarTodosLosDatos();
	}
	public static void ejercicio2() {
		GestionarUsuarios gesUsu = new GestionarUsuarios();
		gesUsu.create("Ana", 30, 'M');
		gesUsu.create("Luis", 31, 'H');
		gesUsu.create("Pedro", 32, 'H');
		gesUsu.mostrarTodos();
		gesUsu.mostrarUnoPorNombre("luis");
		gesUsu.borrarUnoPorNombre("dfdfdfdf");
		gesUsu.borrarUnoPorNombre("luis");
		gesUsu.mostrarTodos();
	}
}
